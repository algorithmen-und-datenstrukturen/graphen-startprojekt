package io.ad.graphs;

public class App {
	public static void main(String[] args) {
		
		DirectedGraph graph = new DirectedGraph();
		
		graph.addNode("1");
		graph.addNode("2");
		graph.addNode("3");
		graph.addNode("4");
		

		graph.addEdge("1", "2", 1);
		graph.addEdge("1", "3", 1);
		graph.addEdge("1", "4", 1);
		graph.addEdge("2", "3", 1);
		graph.addEdge("2", "4", 1);
		graph.addEdge("3", "4", 1);
	
		
	}
}
